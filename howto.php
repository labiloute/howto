<?php
include ('mysql.php');
include ('ldapad.php');

class HowTo {

	function __construct($CONFIG) {
		// Get config from config file
		$this->CONFIG = $CONFIG;
		// Default : connect to mysql
		$this->connect('mysql');
	}

	function connect($sgbd) {
	if ($sgbd=='mysql')	{
		$this->mysql=new mysql($this->CONFIG['dbhost'],$this->CONFIG['dbuser'],$this->CONFIG['dbpass'],$this->CONFIG['dbname']);
		$this->mysql->query_simple("SET NAMES 'utf8'");
		}
	}



	function ConnectLdapAd() {
		// Breaks if configuration not enabled
		if ($this->CONFIG['ldap_enabled']!=true) return False;
		// Connect to ldap database
		if ($_SESSION['user']) {
			$this->ldap=new Ldap($this->CONFIG);
			$this->ldap->connect();
			$this->CacheGroups();
			}
		else
			return $this->Unauthorized();
	//print json_encode($this->ldap->getAllGroups('grpe'));
	}

/***** LDAP Queries *********/
	
	function CacheGroups() {
		if ($_SESSION['user'])
			$_SESSION['UserGroups']=$this->ldap->GetGroups($_SESSION['user'],True); // Get groups the user belongs INCLUDING Primary Group
		}

	function getAllGroups($filter,$additional_groups)
		{
		return $this->ldap->getAllGroups($filter,$additional_groups); // Get all groups from AD, filtering, and include additionnal groups that wouldn't have been return with the filter
		}

/**** GET from DB ***********/
	function GetCatFromSubcatId($subcatid) {// Find Cat id from SubCat ID
		if (!$subcatid) return False;
		$sql='SELECT cat.id as catid from subcat
			LEFT JOIN cat ON subcat.cat_id = cat.id
			WHERE subcat.id = \''.$subcatid.'\'
			LIMIT 1';
		$query=$this->mysql->query_array($sql);
		$r=$query->fetch_array();
		//print_r($r);
		$cat = $r['catid'];
		if (!$cat) return False;
		return $cat;
	}

	function GetSubCatFromQuestionId($id) {// Find SubCat id from Question ID
		if (!$id) return False;
		$sql='SELECT subcat_id as subcatid from question
			WHERE question.id = \''.$id.'\'
			LIMIT 1';
		//echo $sql;
		$query=$this->mysql->query_array($sql);
		$r=$query->fetch_array();
		//print_r($r);
		$subcat = $r['subcatid'];
		if (!$subcat) return False;
		return $subcat;
	}

	function GetViews($id) {// Find how many views this question have
		if (!$id) return False;
		$sql='SELECT views from question
			WHERE question.id = \''.$id.'\'
			LIMIT 1';
		$query=$this->mysql->query_array($sql);
		$r=$query->fetch_array();
		$views = $r['views'];
		return $views;
	}

	function GetCat() {
		// Filter by ACL group if Ldap is enabled - else ignore ACL
		if ($this->CONFIG['ldap_enabled'] && $_SESSION['user']) {
			$sql_where='WHERE';
			foreach ($_SESSION['UserGroups'] as $group) {
				($sql_where=='WHERE')?$txt_or='':$txt_or=' OR ';
				$sql_where.=$txt_or.' (question.acl LIKE \'%'.$group.'%\' 
							OR cat.admins LIKE \'%'.$_SESSION['user'].'%\')';
			}
		}

		$sql='	SELECT cat.id,cat.name,cat.admins from cat
			LEFT JOIN subcat ON cat.id = subcat.cat_id
			LEFT JOIN question ON subcat.id = question.subcat_id
			'.$sql_where.'
			GROUP BY cat.name
			LIMIT 1000';
		//echo $sql;
		$query=$this->mysql->query_array($sql);
		return $query; 
	}
	function GetSubCat($cat) {
		if ($cat==NULL) return False;
		
		// Put WHERE before acl
		$sql_where='WHERE cat_id=\''.$cat.'\'';
		// Filter by ACL group if Ldap is enabled - else ignore ACL
		if ($this->CONFIG['ldap_enabled'] && $_SESSION['UserGroups'] && $_SESSION['user']) {
			$sql_where.=' AND ( (cat.admins LIKE \'%'.$_SESSION['user'].'%\' )';
			foreach ($_SESSION['UserGroups'] as $key=>$group) {
				($key==0)?$txt_or=' OR ':$txt_or=' OR';
				$sql_where.=$txt_or.' question.acl LIKE \'%'.$group.'%\'';
			}
			$sql_where.=')';
		}

		$sql='	SELECT subcat.id,subcat.name,cat.admins 
			FROM subcat 
			LEFT JOIN question on question.subcat_id=subcat.id
			LEFT JOIN cat on subcat.cat_id=cat.id
			'.$sql_where.'
			GROUP BY subcat.name
			 LIMIT 1000';
		//-debug-echo $sql;
		$query=$this->mysql->query_array($sql);
		return $query; 
	}

	// GET MULTIPLE QUESTIONS CONTENT BASED ON SUBCAT ID
	function GetQuestions($subcatid) {
		if ($subcatid==NULL) return False;
		// ACL IF ENABLED
		if ($this->CONFIG['ldap_enabled'] && $_SESSION['UserGroups'] && $_SESSION['user']) {
			// Filter by ACL group if Ldap is enabled - else ignore ACL
			$sql_where='AND ( cat.admins LIKE \'%'.$_SESSION['user'].'%\' ';
			// Loop through the groups the user belongs to verifiy that he is part of acl
			foreach ($_SESSION['UserGroups'] as $key=>$group) {
				($key==0)?$txt_or='OR':$txt_or='OR';
				$sql_where.=$txt_or.' question.acl LIKE \'%'.$group.'%\'';
			}
		$sql_where.=')';
		}

		$sql='SELECT question.id,question.question,question.answer,question.acl,cat.admins from question 
			LEFT JOIN subcat on question.subcat_id=subcat.id
			LEFT JOIN cat on subcat.cat_id=cat.id
			WHERE subcat_id=\''.$subcatid.'\' 
			'.$sql_where.' 
			LIMIT 1000';

		//echo $sql;
		$query=$this->mysql->query_array($sql);
		return $query; 
	}

	// GET ONE QUESTION CONTENT BASED ON QUESTION ID
	function GetQuestion($catid,$subcatid,$questionid) {

		if ($questionid==NULL) return False;
		// ACL IF ENABLED
		if ($this->CONFIG['ldap_enabled'] && $_SESSION['UserGroups'] && $_SESSION['user']) {
		// Filter by ACL group if Ldap is enabled - else ignore ACL
			$sql_where='AND ( cat.admins LIKE \'%'.$_SESSION['user'].'%\' ';
		// Loop through the groups the user belongs to verifiy that he is part of acl
			foreach ($_SESSION['UserGroups'] as $key=>$group) {
				($key==0)?$txt_or='OR':$txt_or='OR';
				$sql_where.=$txt_or.' question.acl LIKE \'%'.$group.'%\'';
			}
		// Close OR file
		$sql_where.=')';
		}

		$sql='SELECT 
				question.id,
				question.question,
				question.answer,
				question.acl,
				question.views,
				cat.admins,
				cat.name as cat_name,
				subcat.name as subcat_name 
			FROM question 
			LEFT JOIN subcat on question.subcat_id=subcat.id
			LEFT JOIN cat on subcat.cat_id=cat.id
			WHERE question.id="'.$questionid.'"
			AND subcat.id="'.$subcatid.'"
			AND cat.id="'.$catid.'"
			'.$sql_where.'
			LIMIT 1';
		$query=$this->mysql->query_array($sql);

		return $query; 
	}

	function PrintableQuestion($catid,$subcatid,$questionid) {
		$today = date("d/m/Y à H")."h".date("i");
		$q=$this->GetQuestion($catid,$subcatid,$questionid);
		$r=$q->fetch_array();
		
		$html='
			<div class="noprint backbutton">
			<a href="?cat='.$_GET['cat'].'&subcat='.$_GET['subcat'].'&question='.$_GET['question'].'">
			<img src="images/back.png" />
			</a>

			</div>
			<div class="company_logo"><img src="'.$this->CONFIG['company_logo'].'" /></div>

			<h4>'._('cat-title').' : '.$r['cat_name'].' | '._('subcat-title').' : '.$r['subcat_name'].' - '._('printed-on').' : '.$today.'</h4>
			<h2 id="title-'.$questionid.'" class="questiontitle">'.$r['question'].'</h2>'.$r['answer'];
		return $html;
	}

	function GetQuestionHistory($id) {
		if ($id==NULL) return False;
		$sql='SELECT * FROM history WHERE question_id=\''.$id.'\' ORDER BY datetime DESC LIMIT 1000';
		$query=$this->mysql->query_array($sql);
		if ($query->num_rows==0) return False;
		return $query;
	}

/********** RETURN DIV ************/
	function DivCat() {
		
		$query=$this->GetCat();
		$r.='<div id="cat">
			<div class="catname tabtitle"><img src="images/cat.png" id="Cat"/> '._('cat-title').'</div>';
		while ($cat=$query->fetch_array())
			{
         if ($this->CheckAdmin($cat['admins']) && $this->CONFIG['ldap_enabled']==true) {
            $txt_acls = '<div class="catadmins" id="catadminsdiv-'.$cat['id'].'" style="display:none;"><input id="catadminsinput-'.$cat['id'].'" catid="'.$cat['id'].'" value="'.$cat['admins'].'" /></div>';
            $txt_buttons='<div class="catbuttons">
                              <img class="savecatadminsbutton" id="savecatadminsbutton-'.$cat['id'].'" catid="'.$cat['id'].'" src="images/ok_grey.png" style="display:none;" />
                              <img class="modifycatadminsbutton" id="modifycatadminsbutton-'.$cat['id'].'" catid="'.$cat['id'].'" src="images/modify_grey.png" />
                           </div>';
         }
         else {
               $txt_acls = False;
               $txt_buttons = False;
            }
			$r.=' <div class="cat" id="cat-'.$cat['id'].'" catid="'.$cat['id'].'">
               '.$txt_acls.'
               '.$txt_buttons.'
               <div class="catname" catid="'.$cat['id'].'" id="catname-'.$cat['id'].'">'.$cat['name'].'</div>
               </div>';
			$allcats[]=$cat['name'];
			}

		$r.='</div>';
		return $r;
	}
	function DivSubCat($cat) {
		if ($cat==NULL) return False;
		$query=$this->GetSubCat($cat);

		$is_admin=$this->CheckAdmin(NULL,$cat);
		if ($query) {
			$txt_h1='<div class="subcatdiv tabtitle">';
			$txt_h2=' '._('subcat-title').'</div>';
			$txt_admin='<img src="images/add_subcat.png" title="'._('subcat-new').'" id="AddSubCat" catid="'.$cat.'"/>';
			($is_admin)?$r.=$txt_h1.$txt_admin.$txt_h2:$r.=$txt_h1.$txt_h2;
			while ($subcat=$query->fetch_array())
				{
				($is_admin)?$txt_remove='<img src="images/del_grey.png" class="delsubcat" title="'._('subcat-delete').'" catid="'.$cat.'"  subcatid="'.$subcat['id'].'" />':$txt_remove=NULL;
				($is_admin)?$txt_modif='<img src="images/modify_grey.png" title="'._('subcat-rename').'" class="modifysubcat"  catid="'.$cat.'"  subcatid="'.$subcat['id'].'" />':$txt_modif=NULL;
				$subcatname='<span id="subcatname-'.$subcat['id'].'" catid="'.$cat.'"  subcatid="'.$subcat['id'].'" class="subcatname active">'.$subcat['name'].'</span>';
				$r.=' <div class="subcatdiv" catid="'.$cat.'" id="subcat-'.$subcat['id'].'" subcatid="'.$subcat['id'].'">
                     <span id="subcatbuttons-'.$subcat['id'].'" catid="'.$cat.'" class="subcatbuttons">'.$txt_remove.$txt_modif.'</span>
                     '.$subcatname.'
                  </div>';
				}
			}
		return $r;
	}
	function DivQuestion($cat,$subcat) {
		if ($subcat==NULL) return False;
		$query=$this->GetQuestions($subcat);
		$is_admin=$this->CheckAdmin(NULL,NULL,$subcat);
		if ($query) {
			$tab_h1='<div class="questiondiv tabtitle">';
			$tab_admin=' <img src="images/add_question.png" title="'._('question-new').'" id="AddQuestion" subcatid="'.$subcat.'" catid="'.$cat.'"/> ';
			$tab_h2=_('howto-title').'</div>';
			($is_admin)?$r.=$tab_h1.$tab_admin.$tab_h2:$r.=$tab_h1.$tab_h2;
			while ($question=$query->fetch_array())
				{
				($is_admin)?$txt_remove='<img src="images/del_grey.png" title="'._('question-delete').'" class="delquestion" questionid="'.$question['id'].'" />':$txt_remove=NULL;
				$r.='<div class="questiondiv" subcat="'.$subcat.'" id="question-'.$question['id'].'" questionid="'.$question['id'].'">
					<span class="questionbuttons">'.$txt_remove.'</span>
					<span class="questionname" subcat="'.$subcat.'" id="question-'.$question['id'].'" questionid="'.$question['id'].'">'.$question['question'].'</span>
					
				</div>';
				$r.=$this->Modal($question['id'],$question['question'],$question['answer'],$question['acl'],$cat,$subcat,$question['admins'],$question['views']);
				}
			}
		return $r;
	}


	function CheckAdmin($admins,$cat=NULL,$subcat=NULL) {
	// If Ldap is disabled return always true (no acl = no admins)
	if (!$this->CONFIG['ldap_enabled'] || !$_SESSION['user']) return true;
	
	// Checking order : $cat (id) / $subcat (id) / $admins (array)
	if ($cat) {
		$sql='SELECT admins FROM cat WHERE id=\''.$cat.'\' LIMIT 1';
		//echo $sql;
		$q=$this->mysql->query_simple($sql);
		$q=$q->fetch_assoc();
		$admins=$q['admins'];
		}
	else if ($subcat) {
		$sql='SELECT admins FROM `subcat` 
			LEFT JOIN cat on cat.id=subcat.cat_id
			WHERE subcat.id=\''.$subcat.'\'';
			$q=$this->mysql->query_simple($sql);
			$q=$q->fetch_assoc();
			$admins=$q['admins'];
		}
	// Check if user is part of admins array
	$s_admins=explode($this->CONFIG['admins_separator'],$admins);
	(in_array($_SESSION['user'],$s_admins))?$is_admin=True:$is_admin=False;
	//print_r($s_admins);
	return $is_admin;
	}





	//******** Display Modal Content
	function Modal($id,$title,$txt,$acl,$cat,$subcat,$admins,$views=NULL,$everyoneisadmin=False) {
	// Is the actual user admin of the category?
	$is_admin=$this->CheckAdmin($admins);

	// Get total views
	if ($views)
		$views += 1;
	else
		$views=$this->GetViews($id);

	// Get the Question History and prepare content
	$hist=$this->GetQuestionHistory($id);
	if ($hist) {
		$txt_hist='
			<hr class="hr-modal-bottom" />
			<div class="history">
				<img src="images/add_grey.png" id="ToggleHistory-'.$id.'" questionid="'.$id.'" class="togglehistory" /> '._('changes-history').'
				<table id="history-'.$id.'" questionid="'.$id.'" class="table-history">';
		$i=0;
		// History rows
		while ($row=$hist->fetch_array())
			{
			$date=$this->daysdiff($row['datetime']);
			($i % 2 != 0)?$c='pair':$c='odd';
			if ($row['username'] == '0') $username = _('Unauthenticated');
			else $username = $row['username'];
			$days = _('days');
			$txt_ago = sprintf(_('ago'),abs($date),$days);
			$txt_hist.='<tr class="'.$c.'"><td title="'.$this->frdate($row['datetime']).'" class="row">'.$txt_ago.'</td><td class="row">'.$username.'</td><td class="row">'._($row['type']).'</td></tr>';
			$i++;
			}
		$txt_hist.='	</table>
			</div>';
		}

	// Construct Modal
	$txt_modal='
	<div id="openModal-'.$id.'" class="modalDialog" questionid="'.$id.'">
		<!--<div class="inner">-->
			<!--<a href="#" title="'._('Close').'" class="close" questionid="'.$id.'">x</a>-->';
	// Permalink
	$txt_permalink.='<a href="?cat='.$cat.'&subcat='.$subcat.'&question='.$id.'" class="permalink"><img src="images/permalink.png" title="'._('permalink').'" class="permalinkbutton" /></a>';
	// Printable verson link
	$txt_print.='<a href="?cat='.$cat.'&subcat='.$subcat.'&question='.$id.'&print=1" class="printable"><img src="images/printer.png" title="'._('printable-version').'" target="new" class="printbutton" /></a>';
         // Button to modify Title
         $txt_modifybutton_title = ''; // Deprecated
         // Button to save title and ACL
         $txt_savebutton_title = '<img src="images/ok_grey.png" id="SaveButtonTitle-'.$id.'" class="savebuttontitle" title="'._('save-changes').'" />';
         // Button to modify content
         $txt_modifybutton_content_top = '<img src="images/modify_grey.png" class="questionmodify" id="questionmodify-top-'.$id.'" style="display:inline;" title="'._('content-change').'"/>';
         $txt_modifybutton_content_bottom = '<img src="images/modify_grey.png" class="questionmodify" id="questionmodify-bottom-'.$id.'" style="display:inline;" title="'._('content-change').'"/>';
         // Button to save content 
         $txt_savebutton_content_top = '<img src="images/ok_grey.png" title="'._('save-changes').'" id="SaveButton-top-'.$id.'" class="savebutton" style="display:none;" />';
         $txt_savebutton_content_bottom = '<img src="images/ok_grey.png" title="'._('save-changes').'" id="SaveButton-bottom-'.$id.'" class="savebutton" style="display:none;" />';
		
		if ($is_admin || $everyoneisadmin) $c_title = 'admin';
		$txt_modal.=' <h2 id="title-'.$id.'" class="questiontitle '.$c_title.'">'.$title.'</h2>';
		//else $txt_modal.='<h2>'.$title.'</h2>';
		if ($this->CONFIG['ldap_enabled']) $txt_modal.='<h3 id="acl-'.$id.'" class="acl" style="display:none;" >
		'._('add-group-to-acl').' :<br />
		<input id="searchgroup-'.$id.'" class="searchgroup" questionid="'.$id.'"/>
		<img src="images/add_question.png" title="'._('add-group-to-acl').'" id="AddGroup-'.$id.'" class="AddGroup" />
		<img src="images/del_grey.png" title="'._('remove-group-to-acl').'" id="RemGroup-'.$id.'" class="RemGroup"><br />
		<input id="inputacl-'.$id.'" class="inputacl" value="'.$acl.'"  questionid="'.$id.'"/>
		'.$txt_savebutton_title.'
		</h3>
		';
		

		$txt_modal.='<div class="buttonscontainer">'.$txt_permalink.$txt_print;
		if ($is_admin || $everyoneisadmin) $txt_modal .= $txt_modifybutton_content_top.$txt_savebutton_content_top;


		$txt_modal.='
		</div>
		<p>
		<div id="questioncontent-'.$id.'" class="questioncontent">
               		'.$txt.'
            	</div>

		<div class="buttonscontainer">
		'.$txt_permalink.$txt_print;
		if ($is_admin || $everyoneisadmin) $txt_modal .=$txt_modifybutton_content_bottom.$txt_savebutton_content_bottom;
		$txt_modal .= '</div></p>';
		//else 
               //$txt_modal.='<p><div class="questioncontent">'.$txt.'</div></p>';
         
		// Add views counter
		$txt_modal.='<hr style="clear:both;" />'.$views.' '._('views').'';
		
         	// Add history for all displays
		$txt_modal.= $txt_hist.'
		<!--</div>-->
	</div>
	';

	return $txt_modal;
	}

	function PageTitle() {
      if ($this->CONFIG['software-title']) $title1 = $this->CONFIG['software-title'];
      else $title1 = _('software-title');
      return '<div id="pagetitle">'.$title1.' - '.$this->CONFIG['owner'].'</div>';

	}

	//******************
	// ADD DATA to DB
	//******************

	function IncrementView($id) {
		$sql='UPDATE question SET views=views + 1
			WHERE id="'.$id.'"';
		$r=$this->mysql->query_simple($sql);
		$error=$this->mysql->last_error;

	}

	function AddSubCat($cat,$text) {
		if ($cat==NULL) return False;
		$text=$this->mysql->escape_string($text);
		// Insert Question in DB
		$sql='INSERT INTO subcat(cat_id,name) VALUES('.$cat.',\''.$text.'\')';
		$query=$this->mysql->query_simple($sql);
		if ($query) {
			$last_id=$this->mysql->last_id;
			if ($last_id) return $last_id;
			}
		else { $this->last_error='Erreur SQL : '.$this->mysql->last_error;
			return False;
			}
	}
	function AddQuestion($cat,$subcat,$text) {
		if ($subcat==NULL) return False;
		// Insert Question
      $text=$this->mysql->escape_string($text);
		$sql='INSERT INTO question(subcat_id,question,answer) VALUES('.$subcat.',\''.$text.'\',\''._('new-question-content').'\')';
		$query=$this->mysql->query_simple($sql);
		if ($query) {
			$last_id=$this->mysql->last_id;
			if ($last_id) {
				// Keep trace of history
				$h=$this->LogHistory($last_id,'Creation');
				return $last_id;
				}
			}
		else { 	$this->last_error='Erreur SQL : '.$this->mysql->last_error;
			return False;
			}
	}

	function LogHistory($question_id,$type='Modif')
		{
		if (!$question_id) return False;
		(!$_SESSION['user'])?$user='0':$user=$_SESSION['user'];
		$sql='INSERT INTO history(question_id,datetime,username,type) VALUES('.$question_id.',NOW(),\''.$user.'\',\''.$type.'\')';
		$query=$this->mysql->query_simple($sql);
		if ($query) {
			$last_id=$this->mysql->last_id;
			if ($last_id) return $last_id;
			}
		
		else {
			$this->last_error='Erreur SQL : '.$this->mysql->last_error;
			return False;
			}

		}

	//******************
	// MODIFY DATA in DB
	//******************

   // Save cat admins changes
   function SaveCatAdmins($cat,$admins) {
      if ($cat==NULL) return False; // If not cat provided, disallow update
      if (!$this->CheckAdmin(NULL,$cat)) return False; // Disallow update if current user is not admin of the category
      $sql='UPDATE cat SET admins="'.$admins.'" WHERE id="'.$cat.'"';
      $r=$this->mysql->query_simple($sql);
      $error=$this->mysql->last_error;
		if ($r) return "Changes saved successfully.";
		else {$this->last_error=$error;return False;}
   }

	// Save SubCat modification
	function ModifySubCat($subcatid,$text) {
		if ($subcatid == NULL || $text == NULL){$this->last_error='SubCatid or Text are NULL'; return False;}
      $text = $this->mysql->escape_string($text);
		$sql='UPDATE subcat SET name=\''.$text.'\' WHERE id=\''.$subcatid.'\' LIMIT 1 ';
		$r=$this->mysql->query_simple($sql);
		$error=$this->mysql->last_error;
		if ($r) return "Changes saved successfully.";
		else {$this->last_error=$error;return False;}
	}

	// Save Changes for Question
	function SaveChanges($id,$html) {
		$html=$this->mysql->escape_string($html);
		// Update Question in DB
		$sql = 'UPDATE question SET answer=\''.$html.'\' WHERE id=\''.$id.'\' LIMIT 1';
		$r=$this->mysql->query_simple($sql);
		$error=$this->mysql->last_error;
		// Keep trace of history
		$h=$this->LogHistory($id);
		if ($r && $h) return "Changes saved successfully.";
		else if ($r && !$h) return "Changes saved successfully, but no history has been saved.";
		else {
			$this->last_error=$error;
			return False;
			}
	}
	
	//Save changes for Title
	function SaveChangesTitle($id,$question,$acl) {
		$question=$this->mysql->escape_string($question);
		$acl=$this->mysql->escape_string($acl);
		// Update Title of Question in DB
		$sql = 'UPDATE question SET question=\''.$question.'\',acl=\''.$acl.'\' WHERE id=\''.$id.'\' LIMIT 1';
		$r=$this->mysql->query_simple($sql);
		$error=$this->mysql->last_error;
		// Keep trace of history
		$h=$this->LogHistory($id,'title-acl-changes');
		if ($r && $h) return "Changes saved successfully.";
		else if ($r && !$h) return "Changes saved successfully, but no history has been saved.";
		else {
			$this->last_error=$error;
			return False;
			}
	}
	//******************
	// REMOVE DATA to DB
	//******************
	function DeleteQuestion($questionid) {
		if ($questionid==NULL) 
			{$this->last_error='QuestionId unespecified in DeleteQuestion'.PHP_EOL;
			return False;}
		$sql1='DELETE question,history
			FROM question
			LEFT JOIN history ON history.question_id = question.id
			WHERE question.id=\''.$questionid.'\'';
		//return $sql;
		$query1=$this->mysql->query_simple($sql1);
		if ($query1) return True;
		else {
			$this->last_error='SQL : '.$this->mysql->last_error.' - Query : '.$sql1;
			return False;
			}
	}
   
	function DeleteSubCat($subcatid) {
		if ($subcatid==NULL) {
			$this->last_error='SubCatId unspecified.';
			return False;}
		$sql1='DELETE subcat,question,history
			FROM subcat
			LEFT JOIN question ON question.subcat_id = subcat.id
			LEFT JOIN history ON history.question_id = question.id
			WHERE subcat.id=\''.$subcatid.'\'
			';
		//return $sql1;
		$query1=$this->mysql->query_simple($sql1);

		if ($query1) return True;
		else {$this->last_error='SQL : '.$this->mysql->last_error.' - Query:'.$sql1;
			return False;
			}
	}

	//******************
	// Display some text
	//******************
	function GetUser() {
		if ($_SERVER['REMOTE_USER']!=NULL) 
			{$user=explode('@',$_SERVER['REMOTE_USER']);
			$_SESSION['user']=$user[0];
			//echo $user[0];
			}
		
	}
	
	function Greeter() {
		//echo $_SESSION['user'];
		if ($_SESSION['user'] != NULL)
			return '<div id="greeter">'._('Hello').' '.$_SESSION['user'].'</div>';
	}


	//*****************
	// Save Changes Submitted
	//*****************


	/************* */
	/******** MISC */
	/************* */
	function daysdiff($date) {
		$now = time();
		$date = strtotime($date);
		$daysdiff = ceil(($date - $now) / (60*60*24));
		return $daysdiff;
	}

	// Date in french format
	function frdate($date) {
		return date('d-m-Y',strtotime($date));
	}



	//****************
	// DISPLAY HEADER / FOOTER
	//****************
	function DisplayHeader($bg,$printdialog=False){
	($bg)?$bgclass='bg':$bgclass='nobg';
	($printdialog)?$printtxt=' onload="window.print()" ':$printtxt='';
	return '<!DOCTYPE html>
	<html class="'.$bgclass.'" >
	    <head>
		<title><-- HowTo --></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<!-- CSS -->

		<link rel="stylesheet" href="css/jquery-ui.css" />
		<link rel="stylesheet" href="css/style.css" />

		<!-- JS -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
		<script type="text/javascript" src="js/jquery-validation.js"></script>
		<script src="ckeditor/ckeditor.js"></script>
		<script src="ckeditor/adapters/jquery.js"></script>
	    </head>
	    <body '.$printtxt.'>
	';
	}
	//footer
	function DisplayFooter(){
	return '
	</body>
	</html>';
	}
	//****************
	// DISPLAY UNAUTHORIZED
	//****************

	function Unauthorized(){
	$txt='<div class="unauthorized">Désolé. Vous n\'êtes pas autorisé(e) à accéder à ce logiciel.</div>';
	$page=$this->DisplayHeader(True);
	$page.=$txt;
	$page.=$this->DisplayFooter();
	echo $page;
	exit;
	}

}
?>
