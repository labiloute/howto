<?php
class mysql {
	function __construct($dbhost,$dbuser,$dbpass,$dbname)
		{
		$this->dbhost=$dbhost;
		$this->dbuser=$dbuser;
		$this->dbpass=$dbpass;
		$this->dbname=$dbname;

		$this->cx = new mysqli($this->dbhost,$this->dbuser,$this->dbpass,$this->dbname);
		if (!$this->cx)
			die ("Connexion error");

		}

	function query_array($sql) {
		$result = $this->cx->query($sql) or die("Erreur de requête Mysql type array : ".$this->cx->error);
		return $result;
	}
	function query_assoc($sql) {
		$result = $this->cx->query($sql) or die("Erreur de requête Mysql type assoc : ".$this->cx->error);
		return $result;
	}
	function query_simple($sql) {

		$result = $this->cx->query($sql) or $this->last_error="Erreur de requête simple Mysql : ".$this->cx->error;
		$this->last_affected_rows = $this->cx->affected_rows;
		$this->last_id = $this->cx->insert_id;
		return $result;
	}
	function escape_string($string) {
		return $this->cx->real_escape_string($string);
	}

}
?>
