<?php
session_start();
include('howto.php');
include('config.php');
include ('locale.php');

$howto=new HowTo($CONFIG);
$howto->GetUser();
$howto->connect('mysql');

// Save Cat admins
if ($_GET['cat'] && $_GET['admins']) {
   echo $howto->SaveCatAdmins($_GET['cat'],$_GET['admins']);
}
// Query SubCat DIV
if ($_GET['cat'] && !$_GET['text'] && !$_GET['subcat']) {
	echo $howto->DivSubCat($_GET['cat']);
	}
// Query Question DIV
if ($_GET['subcat'] && $_GET['cat'] && !$_GET['text']) {
	echo $howto->DivQuestion($_GET['cat'],$_GET['subcat']);
	}
// Create SubCat
if ($_GET['cat'] && $_GET['text'] && !$_GET['subcat']) {
	echo $howto->AddSubCat($_GET['cat'],$_GET['text']);
	}
// Modify SubCat
if ($_GET['cat'] && $_GET['text'] && $_GET['subcat'] && $_GET['action']=='ValidModificationSubCat') {
	echo $howto->ModifySubCat($_GET['subcat'],$_GET['text']);
	}

// Create Question
if ($_GET['cat'] && $_GET['subcat'] && $_GET['text'] && $_GET['action']=='ValidNewQuestion') {
	echo $howto->AddQuestion($_GET['cat'], $_GET['subcat'], $_GET['text']);
	}
// Save Question Content
if ($_POST['questionid'] && $_POST['questionhtml']) {
	echo $howto->SaveChanges($_POST['questionid'],$_POST['questionhtml']);
	}
// Save Question Title
if ($_POST['questionid'] && $_POST['questiontitle']) {
	echo $howto->SaveChangesTitle($_POST['questionid'],$_POST['questiontitle'],$_POST['questionacl']);
	}
// Display Modal
if ($_POST['questionid'] && $_POST['title'] && $_POST['text'] && $_POST['GetModal']) {
	echo $howto->Modal($_POST['questionid'],$_POST['title'],$_POST['text'],NULL,NULL,True);
	}
// Delete Something
if ($_POST['id'] && $_POST['Delete'] && $_POST['type']) {
	if ($_POST['type']=='question') {
		echo $r=$howto->DeleteQuestion($_POST['id']);
		}
	else if ($_POST['type']=='subcat') {
		echo $r=$howto->DeleteSubCat($_POST['id']);
		}
	if (!$r) echo 'Suppression de type '.$_POST['type'].' : '.$howto->last_error;
	}
// Get groups - LDAP
if ($_POST['search_group'] && $_POST['json']  && $_GET['callback'] && $CONFIG['ldap_enabled']) {
	$howto->ConnectLdapAd();
	echo $_GET['callback']."(".json_encode($howto->getAllGroups($CONFIG['groups_prefix'].$_POST['search_group'],$CONFIG['additional_groups'])).")";
	}
// Get SubCat from Question
if ($_POST['questionid'] && $_POST['getsubcatid']) {
	echo $howto->GetSubCatFromQuestionId($_POST['questionid']);
	}
// Get Cat from Subcat
if ($_POST['subcatid'] && $_POST['getcatid']) {
	echo $howto->GetCatFromSubcatId($_POST['subcatid']);
	}
// Add one view if modal is displayed
if ($_POST['incrementview']) {
	$howto->IncrementView($_POST['incrementview']);
	}
?>
