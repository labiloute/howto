��    ,      |  ;   �      �     �     �     �     �     �     �     �     �     �                          &     *     ;  	   ?     I     Y     h     m     �     �     �  	   �     �  
   �     �     �     �               "     6     C     R  
   `     k     y     �     �     �     �  A  �     �  	   �                 
        '     /     3     <     B     S     W     \  ,   `     �  
   �     �     �     �  R   �  d   -     �  \   �  b   �     `	     s	     �	     �	     �	     �	  &   �	  -   
     6
     R
     l
     �
     �
  !   �
     �
  #   �
                  	                            #      %   &                           "                     +       !         *   '                                  
      (                           ,   )   $           Close Creation Deleting Hello Howto Loading New No Question Title Unauthenticated View Views Yes add-group-to-acl ago cat-title changes-history content-change days delete-question-msg delete-subcat-msg howto-title new-question-content permalink printable-version printed-on question-delete question-new question-rename question-save reading-acl remove-group-to-acl save-changes software-title subcat-delete subcat-new subcat-rename subcat-save subcat-title title-acl-changes view views Plural-Forms: nplurals=2; plural=(n > 1);
Project-Id-Version: Simple Howto
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Olivier BILHAUT <bilhautolivier@neuf.fr>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
X-Generator: Poedit 1.8.11
 Fermer Création Suppression Bonjour Howto Chargement Nouveau Non Question Titre Non authentifié Vue Vues Oui Autoriser ce groupe à  lire cette question il y à %u %s Catégorie Historique des changements Changer le contenu jours Voulez-vous supprimer cette question ? Attention, cette action est irréversible ! Voulez-vous supprimer cette sous-catégorie ? Attention, ceci supprime toutes les questions liées ! Comment ... ? Ceci est le contenu du tutoriel.<br />Cliquez sur un des boutons de droite pour le modifier. Le lien permanent vers ce document. Pour l'envoyer, cliquez-droit, puis -Copier l'adresse du lien- Version imprimable Document imprimé le Supprimer la question Créer une question Renomer la question Sauvegarder cette question Groupes autorisé à lire ce document Interdire à ce groupe de lire cette question Sauvegarder les changements Procédures Informatiques Supprimer la sous-catégorie Créer une sous-catégorie Renommer la sous-catégorie Sauvegarder cette sous-catégorie Sous-categorie Changer le titre ou les permissions vue vues 