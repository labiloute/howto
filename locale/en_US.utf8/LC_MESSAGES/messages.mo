��    ,      |  ;   �      �     �     �     �     �     �     �     �     �     �                          &     *     ;  	   ?     I     Y     h     m     �     �     �  	   �     �  
   �     �     �     �               "     6     C     R  
   `     k     y     �     �     �     �  B  �     �     �          
                    "     %     .     4     D     I     O  &   S  	   z     �     �     �     �  R   �  |        �  `   �  8   �     +	     =	     Q	     a	     q	     �	  $   �	  )   �	     �	     �	     

     
     0
     C
     T
     `
     t
     y
        	                            #      %   &                           "                     +       !         *   '                                  
      (                           ,   )   $           Close Creation Deleting Hello Howto Loading New No Question Title Unauthenticated View Views Yes add-group-to-acl ago cat-title changes-history content-change days delete-question-msg delete-subcat-msg howto-title new-question-content permalink printable-version printed-on question-delete question-new question-rename question-save reading-acl remove-group-to-acl save-changes software-title subcat-delete subcat-new subcat-rename subcat-save subcat-title title-acl-changes view views Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Simple Howto
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Olivier BILHAUT <bilhautolivier@neuf.fr>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en_US
X-Generator: Poedit 1.8.11
 Close Creation Deleting Hello Howto Loading New No Question Title Unauthenticated View Views Yes Allow this group to read this question %u %s ago Category History of changes Change content days Are you sure you want to delete this question ? Carefull, this action in one-way ! Are you sure you want to delete this subcategory ? Careful, this action in one-way and will delete all contained questions ! How to ... ? This is the content of the tutorial. <br /> Click on the buttons on the right side to modify it. Permalink. Right click and select : -Copy link location- Printable version Document printed on Delete question Create question Rename question Save question name Groups allowed to read this question Disallow this group to read this question Save changes Welcome to simple Howto ! Delete Subcategory Create Subcategory Rename Subcategory Save Subcategory Subcategory Change title or ACL view views 