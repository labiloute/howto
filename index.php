<?php
session_start();
#ini_set('display_errors','1'); # for debugging
//clearstatcache();
include('config.php');
include('howto.php');
include('locale.php');

/****Object creation*****/

$howto=new HowTo($CONFIG);
echo $howto->GetUser();
echo $howto->ConnectLdapAd();

/* Display */
if ($_GET['print']) {
	echo $howto->DisplayHeader(False,True);
	echo $howto->PrintableQuestion($_GET['cat'],$_GET['subcat'],$_GET['question']);
	}
else {
	echo $howto->DisplayHeader(True,False);
	echo $howto->Greeter();
	echo $howto->PageTitle();
	echo $howto->DivCat();
	echo '<div id="subcat"></div>';
	echo '<div id="question"></div>';
	echo '<div id="dialog" title="Basic dialog">
	<p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the \'x\' icon.</p>
	</div>';
	/* Javascript */
	include('script.js.php');
}

echo $howto->DisplayFooter();
?>
