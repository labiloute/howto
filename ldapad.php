<?php
// Version 10 : ajout d'une fonction pour chercher un groupe par son ID en cherchant par la fin de son SID (lent). Utilisé pour retrouver le PRIMARY GROUP d'un utilisateur qui n'était jusqu'à lors pas disponible
// Version 9 : modification de la fonction getAllGroups pour filtrer mais ajouter des exceptions au filtre
// Version 8 : ajout de la fonction getAllGroups qui parcours l'ad pour tous les noms de groupes (avec possibilité de filtrer)
// Version 7 : ajout de la fonction CountPeople pour compter le nombre de personnes ayant un prenom ou un nom particulier
// Version 6 : amélioration de la fonction getDN et ajout de la fonction getGroups
// version 5 : ajout de la fonction getAttribute et getAttributes pour rechercher un attribut specifique à un utilisateur ou plusieurs attributs
// Version 4 : correction dans la fonction ckusergroup_ad et ajout de getUsername
// Version 3 : ajout de la fonction ldap_bind
// Version 2 : ajout de la function qui renvoie dans un tableau la liste des utilisateurs d'un groupe
//  Et sous forme de classe avec les parametres de l'AD fournis dans $CONFIG

class Ldap {

	function __construct($CONFIG) {
		$this->CONFIG = $CONFIG;
		//echo $_SERVER['REMOTE_USER'];
	}

	function connect() {
//		$this->ad = ldap_connect("ldap://{$this->CONFIG['ad_host']}.{$this->CONFIG['ad_domain']}") or die('<br />Impossible de joindre le serveur ldap.<br />');
		$this->ad = ldap_connect("ldap://{$this->CONFIG['ad_domain']}") or die('<br />Impossible de joindre le serveur ldap.<br />');
		ldap_set_option($this->ad, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($this->ad, LDAP_OPT_REFERRALS, 0);
		@ldap_bind($this->ad, $this->CONFIG['ad_bind_user']."@".$this->CONFIG['ad_domain'], $this->CONFIG['ad_bind_passwd']) or die('<br />You requested login with a Ldap account but we are unable to join your Ldap server :  '.$this->CONFIG['ad_host'].'. Please check configuration.<br />');
		return true;
	}

	function ldap_bind($username,$password) {
		// Concat AD domain if not given
		if (!strpos($username, '@'))
			$username = $username.'@'.$this->CONFIG['ad_domain'];
		//echo $username;
		$ldapbind = @ldap_bind($this->ad, $username, $password);
		if ($ldapbind) return True;
		else return False;
		}
	function ckusergroup_ad($user,$group) {
		//check if user contain an @ char, then split and keep only the username :
		if (strpos($user, '@')!=false)
			{
			$user_splited=explode('@',$user);
			$user=$user_splited[0];
			}
		//echo $user;
		//ad bind
		$userdn = $this->getDN($user);
		if ($this->checkGroupEx($userdn, $this->getDN($group))) {
			//if ($this->checkGroup($userdn, $this->getDN($group)))
		    		//echo "You're authorized as ".$this->getCN($userdn);
			return true;
		} else {
		    //echo 'Authorization failed';
			return false;
		}

	}
	/*
	* This function searchs in LDAP tree ($ad -LDAP link identifier)
	* entry specified by samaccountname and returns its DN or epmty
	* string on failure.
	*/
	function getDN($samaccountname) {
	//check if user contain an @ char, then split and keep only the username :
	    if (strpos($samaccountname, '@')!=false)
		{
		$user_splited=explode('@',$samaccountname);
		$samaccountname=$user_splited[0];
		}
	    $attributes = array('dn');
	    $result = ldap_search($this->ad, $this->CONFIG['ad_basedn'],"(samaccountname={$samaccountname})", $attributes);
	    if ($result === FALSE) { return ''; }
	    $entries = ldap_get_entries($this->ad, $result);
	    if ($entries['count']>0) { return $entries[0]['dn']; }
	    else { return ''; };
	}

	/*
	* This function retrieves and returns CN from given DN
	*/
	function getCN($dn) {
	    preg_match('/[^,]*/', $dn, $matchs, PREG_OFFSET_CAPTURE, 3);
	    return $matchs[0][0];
	}

	/*
	* This function return the username without the domain name 
	*/
	function getUsername($fqu) {
		if (strpos($fqu, '@')!=false)
			{
			$user_splited=explode('@',$fqu);
			$user=$user_splited[0];
			return $user;
			}	
	}
	/*
	* This function checks group membership of the user, searching only
	* in specified group (not recursively).
	*/
	function checkGroup($userdn, $groupdn) {
	    $attributes = array('members');
	    $result = ldap_read($this->ad, $userdn, "(memberof={$groupdn})", $attributes);
	    if ($result === FALSE) { return FALSE; };
	    $entries = ldap_get_entries($this->ad, $result);
	    return ($entries['count'] > 0);
	}

	/*
	* This function checks group membership of the user, searching
	* in specified group and groups which is its members (recursively).
	*/
	function checkGroupEx($userdn, $groupdn) {
	    $attributes = array('memberOf');
		//echo $ad.' '.$userdn;
	    $result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attributes);
	    if ($result === FALSE) { return FALSE; };
	    $entries = ldap_get_entries($this->ad, $result);
	    if ($entries['count'] <= 0) { echo "<br />Erreur -1 dans checkGroupEx</br />";return FALSE; };
	    if (empty($entries[0]['memberof'])) {  return FALSE; } else {
		for ($i = 0; $i < $entries[0]['memberof']['count']; $i++) {
		    if ($entries[0]['memberof'][$i] == $groupdn) { return TRUE; }
		    elseif ($this->checkGroupEx($entries[0]['memberof'][$i], $groupdn)) { return TRUE; };
		};
	    };
	    //-debug-echo "<br />Erreur chelou dans checkGroupEx</br />";
	    return FALSE;
	}

	function getMembers($group) {
		$query = ldap_search($this->ad, $this->CONFIG['ad_basedn'],"cn=$group");
		// Read all results from search
		$data = ldap_get_entries($this->ad, $query);
		$dirty = 0;
		foreach($data[0]['member'] as $member) {
			if($dirty == 0) {
				$dirty = 1;
			} else {
				$member_dets = $this->explode_dn($member);
				$members[] = str_replace("CN=","",$member_dets[0]);
			}
		}
		return $members;
	}

	function getAccountExpirationDate($username) {
		$attributes = array('accountExpires');
		$userdn = $this->getDN($username);
		//echo $username.' '.$ad_basedn.' = '.$userdn.'<br />';
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attributes);
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		//-debug-echo "Windows timestamp : ".$entries[0]['accountexpires'][0]."<br />";
		if (empty($entries[0]['accountexpires'][0])) 
			{return FALSE; } 
		else if ($entries[0]['accountexpires'][0]=='9223372036854775807' OR $entries[0]['accountexpires'][0]=='0')
			{return 0;}
		else
			{return $this->ldaptimestamp_convert($entries[0]['accountexpires'][0]);}

	}

	// Get a specific attribute of a user DN - /!\ return an array with attributes names in lowercase !
	function getAttribute($username,$attribute) {
		//echo $username.$attribute;
		$userdn = $this->getDN($username);
		//echo "UserDn : ".$userdn;
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', array($attribute));
		if ($result === FALSE) { return FALSE; }
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries[0]);
		return $entries[0][$attribute][0];
		
	}

	//*********
	// This function returns all groups, eventually filtered by pattern $filter if given
	//*********
	// /!\ Warning : there is an intermediary function in howto.php
	function getAllGroups($filter=NULL,$additional_groups=NULL) {
	$fields=array('cn');
	// A filter as been given
	if ($filter) {
		// But some additional groups can be added this way. They bypass the filter.
		if (!empty($additional_groups)) {
			$txt_or='(|';$txt_or_end=')';
			foreach ($additional_groups as $group) {
				//$a[]='Ce groupe :'.$group;
				$txt_additional_groups.='(cn='.$group.')';
				}
			}
		$ldap_query='(&(objectclass=group)'.$txt_or.'(cn=*'.$filter.'*)'.$txt_additional_groups.')'.$txt_or_end;
		//$a[]=$ldap_query;
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'],$ldap_query,$fields);

		}
	else
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'], '(objectclass=group)',$fields);
	if ($result === FALSE) { return FALSE; }
	$entries = ldap_get_entries($this->ad, $result);
	foreach ($entries as $entry) {
		if ($entry['cn'][0]!=NULL)
			$a[]=$entry['cn'][0];
		}

	//print_r($entries);
	return $a;

	}

	// Get a an array of attributes of a user DN -  /!\ return an array with attributes names in lowercase !
	function getAttributes($username,$attribute) {
		$userdn = $this->getDN($username);
		//echo $userdn;
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attribute);
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		return $entries[0];
		
	}


	function getEmailList() {
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'], '(mail=*)', array('mail','sn','givenname'));
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries);
		return $entries;
		
	}

	function explode_dn($dn, $with_attributes=0)
	{
	    $result = ldap_explode_dn($dn, $with_attributes);
	    //translate hex code into ascii again
	    if ($result) {
			//foreach($result as $key => $value) $result[$key] = preg_replace("/\\\([0-9A-Fa-f]{2})/e", "''.chr(hexdec('\\1')).''", $value);
			//foreach($result as $key => $value) $result[$key] = preg_replace_callback("/\\\([0-9A-Fa-f]{2})/", "''.chr(hexdec('\\1')).''", $value);
                        foreach($result as $key=>$value) $result[$key] = preg_replace_callback('/\\\([0-9A-Fa-f]{2})/', function ($matches) { return chr(hexdec($matches[1])); }, $value);
			return $result;
			}
	    return false;
	}

	function ldaptimestamp_convert($ldaptimestamp) {
		$winSecs       = (int)($ldaptimestamp / 10000000); // divide by 10 000 000 to get seconds
		$unixTimestamp = $winSecs - ((1970-1601) * 365.242190) * 86400; // 1.1.1600 -> 1.1.1970 difference in seconds
		return intval($unixTimestamp);
	}

	function unbind() {
		ldap_unbind($this->ad);
	}

	// ***************
	// Function to retrieve group name by GID - Need to browse all groups. Be carefull ! IT IS SLOW !
        function getGroupFromid($id,$restrict_buildin=False) {
		if ($this->CONFIG['ad_basedn_buildingroups'] && $restrict_buildin)
			$basedn=$this->CONFIG['ad_basedn_buildingroups'];
		else
			$basedn=$this->basedn;
		$ldapattributes = array("objectSid","samaccountname");
		$filter="(ObjectClass=group)";
		//echo $basedn;
		$q = ldap_search($this->ad, $basedn,$filter,$ldapattributes);
		$r = ldap_get_entries($this->ad, $q);
		//print_r($r).'<br /><br />';
		foreach ($r as $g) {
			//print_r($g).'<br /><br />';
			$arr_gid=explode("-",$this->bin_to_str_sid($g['objectsid'][0]));
			//echo $arr_gid[7].'compared to '.$id.PHP_EOL;
			if($arr_gid[7]==$id) {return $g['samaccountname'][0];}
			}
		

		//var_dump($entry);
	}
	//***********
	// Function to get groups the user belongs, with an option to get PRIMARY GROUP (which takes more time)
	// **********
	function getGroups($username,$GetPrimary=False) {
		$raw=$this->getAttributes($username,array('memberOf','primaryGroupID'));
		$primary=($raw[primarygroupid][0]);
		//echo $primary;
		$allgroups=array();
		foreach ($raw['memberof'] as $group) 
			{
			//$group=explode('')
			//echo $group;
			$explodedngroup=$this->explode_dn($group);
//print_r($explodedngroup);
			$splitgroup=explode("=",$explodedngroup[0]);
			if ($splitgroup[1]!="" && $splitgroup[1]!=NULL)
				$allgroups[]=$splitgroup[1];
			}
		// Append primary Group
		if ($GetPrimary) {
			$allgroups[]=$this->getGroupFromid($primary,True);
			}
		//print_r($allgroups);
		return $allgroups;
		}

	// **************
	// Write in LDAP server
	// **************
	function SetValue($username,$value_name,$value)
		{
		if ($valuename=='mobile')
			$userdata[$value_name][0]=$value;
		else
			$userdata[$value_name]=$value;
		$userdn = $this->getDN($username);
		$mod=ldap_modify($this->ad, $userdn, $userdata);
		return $mod;
		}

	function CountPeople($firstname,$lastname=NULL)
		{
		if ($firstname && $lastname)
			$filter="(|(sn=$lastname*)(givenName=$firstname*))";
		else {
			$filter="(givenName=*$firstname*)";
			//echo $filter;
			}
			
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'],$filter,array('sn'));
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries);
		return $entries['count'];
		}

	// ******************
	// Function to convert AD Binary HEx to string and invert
	// ******************

	// Returns the textual SID
	function bin_to_str_sid($binsid) 
	{
	    $hex_sid = bin2hex($binsid);
	    $rev = hexdec(substr($hex_sid, 0, 2));
	    $subcount = hexdec(substr($hex_sid, 2, 2));
	    $auth = hexdec(substr($hex_sid, 4, 12));
	    $result    = "$rev-$auth";

	    for ($x=0;$x < $subcount; $x++) {
		$subauth[$x] = 
		    hexdec($this->little_endian(substr($hex_sid, 16 + ($x * 8), 8)));
		$result .= "-" . $subauth[$x];
	    }

	    // Cheat by tacking on the S-
	    return 'S-' . $result;
	}

	// Converts a little-endian hex-number to one, that 'hexdec' can convert
	function little_endian($hex) 
	{
	    $result = "";

	    for ($x = strlen($hex) - 2; $x >= 0; $x = $x - 2) 
	    {
		$result .= substr($hex, $x, 2);
	    }
	    return $result;
	}


	// This function will convert a binary value guid into a valid string.
	function bin_to_str_guid($object_guid) 
	{
	    $hex_guid = bin2hex($object_guid);
	    $hex_guid_to_guid_str = '';
	    for($k = 1; $k <= 4; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 8 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-';
	    for($k = 1; $k <= 2; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 12 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-';
	    for($k = 1; $k <= 2; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 16 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 16, 4);
	    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 20);

	    return strtoupper($hex_guid_to_guid_str);
	}

}
?>
