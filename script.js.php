<?php
echo '
<script type="text/javascript">
$(document).ready(function()
	{
	//*******
	// Init
	//*******

	var catid=null
	var subcatid=null
	var questionid=null
	var maxsize=16;// Max line height in px - roughly eq to 1em
	var ck=[];// Container for all CkEditor instances

	//*******
	//  Functions
	//*******
	function getURLParameter(name) {
  		return decodeURIComponent((new RegExp(\'[?|&]\' + name + \'=\' + \'([^&;]+?)(&|#|;|$)\').exec(location.search)||[,""])[1].replace(/\+/g, \'%20\'))||null;
	}

	function getBaseUrl() {
		url = window.location.href.replace("#","");
		partsofurl=url.split("?");
		base=partsofurl[0];
		return base;
	}

	// Monitor back and next button
	function changestate(state) {
	    if(state !== null) {
		$("div").text(state.url);
		}
	}

	$(window).on("popstate", function(e) {
		//alert("popstate");
		changestate(e.originalEvent.state);
		loadpageonurl();
		});


	//*******
	// History
	//*******
	$(\'body\').on(\'click\',\'.togglehistory\',function() {
		var questionid = $(this).attr(\'questionid\'); // get Question ID
		status=$(\'#history-\'+questionid).css(\'display\');
		if (status==\'none\') {
			$(\'#history-\'+questionid).fadeIn();
			$(\'#ToggleHistory-\'+questionid).attr(\'src\',\'images/rem_grey.png\');
			}
		else
			{
			$(\'#history-\'+questionid).fadeOut();
			$(\'#ToggleHistory-\'+questionid).attr(\'src\',\'images/add_grey.png\');
			}
	});


   	//*************
	// DIALOG BOX - POPUP TO DELETE AN ELEMENT
   	//*************
	function DeleteSomething(title,message,id,type){
		if (type==null) console.log("Error in ConfirmDeletion. type cannot be null");
	    $(\'<div></div>\').appendTo(\'body\')
		            .html(\'<div><h6>\'+message+\'</h6></div>\')
		            .dialog({
		                modal: true, title: title, zIndex: 10000, autoOpen: true,
		                width: \'auto\', resizable: false,
		                buttons: {
		                    '._('Yes').': function () {
		                        $(this).dialog("close");
					$.ajax({
						url: \'ajax.php\',
						data: {
							id: id,
							type: type,
							Delete: true
							},
						error: function(html) {
							alert(html);
							console.log(\'An error (5) has occurred : \' + html);
							},
						success: function(html) {
							// Remove ID from given type
							$(\'#\'+type+\'-\' + id).fadeOut( function() { $(this).remove(); }); 
							// Specific Case of subcat / hide questions as well
							if (type==\'subcat\') { 
								$(\'#question\').fadeOut().empty().fadeIn();
								}
							//-debug-console.log(html);
							},
					type: \'POST\',
					timeout : 5000
					});
		                    },
		                    '._('No').': function () {
		                        $(this).dialog("close");
		                    }
		                },
		                close: function (event, ui) {
		                    $(this).fadeOut( function() { $(this).remove(); });
				}
			});
	    };

	//**********
	// "x" Buttons
	//**********
	// Delete Question
	$(\'body\').on(\'click\',\'.delquestion\',function() {
		var questionid = $(this).attr(\'questionid\'); // get Question ID
		DeleteSomething(\''._('Deleting').'\',\''._('delete-question-msg').'\',questionid,\'question\');
	});

	// Delete SubCat
	$(\'body\').on(\'click\',\'.delsubcat\',function() {
		var subcatid = $(this).attr(\'subcatid\'); // get SubCat ID
		DeleteSomething(\''._('Deleting').'\',\''._('delete-subcat-msg').'\',subcatid,\'subcat\');
	});



	//**********
	// "+" Buttons
	//**********

	//--------
	// Subcat
	//--------

	// Clear input box on focus
	$(\'body\').on(\'focus\',\'.subcatdiv input\',function() {
			newlabel = "'._('New').'";
			if ($(this).val() == newlabel) $(this).val("");
		});

	// Display new subcategory div
	$(\'body\').on(\'click\',\'#AddSubCat\',function() {
		catid=$(this).attr(\'catid\');
		newlabel = "'._('New').'";
		newline=\'<div class="subcatdiv" id="subcat-" catid="\' + catid + \'"><input type="text" id="InputNewSubCat" value="\'+newlabel+\'" catid="\' + catid + \'" /><img src="images/ok_grey.png" id="ValidNewSubCat" catid="\' + catid + \'" title="'._('subcat-save').'" /></div>\';
		$(\'#subcat\').append(newline);
		
	});

   // Trigger click Enter key
   $(\'body\').on(\'keypress\',\'#InputNewSubCat\',function(e) {
      var key = e.which;
      if(key == 13)  // the enter key code
         {
            $(\'#ValidNewSubCat\').click();
            return false;  
         }
      });

	// Save new Subcategory on click
	$(\'body\').on(\'click\',\'#ValidNewSubCat\',function() {
		var catid = $(this).attr(\'catid\');
		var text = $(this).prev().val();
		//alert("Cat ID : "+catid+" Text : "+text);
		if (catid == null || text == null) return false;
		$(\'#subcat\').hide();
		$.ajax({
			url: \'ajax.php\',
			data: {
				cat: catid,
				text : text
			},
			error: function(html) {
				alert(html);
				console.log(\'An error (4) has occurred : \' + html);
			},
			success: function(html) {
				// REFRESH SUBCAT LIST
				//alert("New subcat id :"+subcatid);
				$.ajax({
					url: \'ajax.php\',
					data: {
						cat: catid
					},
					error: function(html2) {
						alert("Error:" + html2);
						console.log(\'An error (3) has occurred : \' + html2);
					},
					success: function(html2) {
						$(\'#subcat\').fadeOut().html(html2).fadeIn();
					},
				type: \'GET\',
				timeout : 5000
				});
			},
		type: \'GET\',
		timeout : 5000
		});
	});

	//------------------
	// Questions -------
	//------------------

	// Clear input box on focus
	$(\'body\').on(\'focus\',\'.questiondiv input\',function() {
			newlabel = "'._('New').'";
			if ($(this).val() == newlabel) $(this).val("");
		});

	// Display new Question Div
	$(\'body\').on(\'click\',\'#AddQuestion\',function() {
		var subcatid=$(this).attr("subcatid");
		var catid=$(this).attr("catid");
		//alert("Cat : "+catid+" SubCat : "+subcatid);
		newline=\'<div class="questiondiv" id="question-" subcatid="\' + subcatid + \'" catid="\' + catid + \'"><input type="text" id="InputNewQuestion" value="'._('New').'" catid="\' + catid + \'" subcatid="\' + subcatid + \'" /><img src="images/ok_grey.png" id="ValidNewQuestion" catid="\' + catid + \'" subcatid="\' + subcatid + \'"  title="'._('question-save').'" /></div>\';
		$(\'#question\').append(newline);
		
	});

   // Trigger click Enter key
   $(\'body\').on(\'keypress\',\'#InputNewQuestion\',function(e) {
      var key = e.which;
      if(key == 13)  // the enter key code
         {
            $(\'#ValidNewQuestion\').click();
            return false;  
         }
      });

	// Save new question on click
	$(\'body\').on(\'click\',\'#ValidNewQuestion\',function() {
		var subcatid = $(this).attr(\'subcatid\');
		var catid = $(this).attr(\'catid\');
		var text = $(this).prev().val();
		if (subcatid == null || text == null) return false;
		// CREATE NEW QUESTION VIA AJAX
		$.ajax({
			url: \'ajax.php\',
			data: {
				cat: catid,
				subcat: subcatid,
				text: text,
				action:\'ValidNewQuestion\'
			},
			error: function(html) {
				alert("Error:" + html);
				console.log(\'An error has occurred : \' + html);
			},
			success: function(html) {
					// REFRESH QUESTION LIST
					//alert (subcatid);
					$.ajax({
						url: \'ajax.php\',
						data: {
							cat : catid,
							subcat: subcatid
						},
						error: function(html2) {
							alert("Error:" + html2);
							console.log(\'An error (2) has occurred : \' + html2);
						},
						success: function(html2) {
							//alert("Success2:" + html2);
							$(\'#question\').html(html2);
						},
					type: \'GET\',
					timeout : 5000
					});
			},
		type: \'GET\',
		timeout : 5000
		});
	});



	//**********
	// DISPLAY SUBCAT
	//**********
	function DisplaySubCat(catid, callback) {

		$.ajax({
			url: \'ajax.php\',
			data: {
				cat: catid
			},
			error: function() {
			$(\'#subcat\').html(\'<p>An error has occurred</p>\');
			},
			success: function(html) {
            			$(\'#subcat\').html(html);
            			catdiv = document.getElementById("cat-"+catid);
            			$(\'.cat\').removeClass("selected");
				$(catdiv).addClass("selected");
				var rowCount = $(\'.subcatdiv\').length;
				size = Math.round(screen.height / (rowCount * 4.5));
				realsize=Math.min(size,maxsize);
				$(\'.subcatdiv\').css({"line-height":realsize+"px","font-size":realsize+"px"});
			},
		type: \'GET\',
		timeout : 5000
		});
	   	$(\'#question\').html("");
		callback && callback();
	}

	// QUERY FOR SUBCAT
	$(\'.catname\').click(function(e) {
		e.preventDefault();
		var catid=$(this).attr("catid");
		//alert(catid);
		DisplaySubCat(catid);
		//alert(cleanurl(window.location.href,"catid"));
		window.history.pushState("'._('Howto').' : '._('cat-title').' "+catid, "'._('Howto').' : '._('cat-title').' "+catid, getBaseUrl()+"?cat="+catid);
	});


	//***********
	// MODIFY SUBCAT
	// **********

	// Switch to input field
	$(\'body\').on(\'click\',\'.modifysubcat\',function() {
		var subcatid=$(this).attr("subcatid");
		var catid=$(this).attr("catid");
		subcatname=$("#subcatname-" + subcatid).text();
      		$("#subcatname-"+subcatid).removeClass("active");
		$("#subcatname-"+subcatid).html(\'<input id="modify-\'+subcatid+\'" subcatid="\'+subcatid+\'" class="input-modify" value="\'+subcatname+\'"/><img src="images/ok_grey.png" class="ValidModificationSubCat" id="ValidModificationSubCat-\' + subcatid + \'" catid="\' + catid + \'" subcatid="\'+subcatid+\'" />\')
	});


   // Trigger click Enter key
   $(\'body\').on(\'keypress\',\'[id^=modify-]\',function(e) {
      var subcatid = $(this).attr("subcatid");
      var key = e.which;
      if(key == 13)  // the enter key code
         {
            $(\'#ValidModificationSubCat-\'+subcatid).click();
            return false;  
         }
      });

	// Save field value on click
	$(\'body\').on(\'click\',\'.ValidModificationSubCat\',function() {
		subcatname=$(this).prev(\'input\').val();
		subcatid=$(this).attr("subcatid");
		catid=$(this).attr("catid");
		//console.log("Will update catid "+catid+" , subcatid "+subcatid+" with value "+subcatname);
		$.ajax({
			url: \'ajax.php\',
			data: {
				cat: catid,
				subcat: subcatid,
				text: subcatname,
				action:\'ValidModificationSubCat\'
			},
			error: function(html) {
				console.log(\'An error has occurred saving your subcatname: \' + html);
			},
			success: function(html) {
				//-debug-console.log(\'Success saving your changes : \'+html)
				//-debug-console.log(\'Refreshing cat : \'+catid)
				DisplaySubCat(catid);
            			$("#subcatname-"+subcatid).addClass("active");
			},
		type: \'GET\',
		timeout : 5000
		});
	});

	//**********
	// DISPLAY QUESTION LIST
	//**********

	function DisplayQuestions(catid,subcatid, callback) {
	   //-debug-console.log("Cat : "+catid+" SubCat " + subcatid);
	   if (subcatid == null || catid == null) return false;
	   $.ajax({
	      url: \'ajax.php\',
		data: {
			cat : catid,
			subcat: subcatid
		},
	      error: function() {
		 $(\'#question\').html(\'<p>An error has occurred</p>\');
	      },
	      success: function(html) {
		$(\'#question\').html(html);
		callback && callback();
		subcatdiv = document.getElementById("subcat-"+subcatid);
		$(\'.subcatdiv\').removeClass("selected");
		$(subcatdiv).addClass("selected");
		var rowCount = $(\'.questiondiv\').length;
		size = Math.round(screen.height / (rowCount * 4.5));
		realsize=Math.min(size,maxsize);
		$(\'.questiondiv\').css({"line-height":realsize+"px","font-size":realsize+"px"});
	      },
	      type: \'GET\',
	      timeout : 5000
	   });

	}

	// QUERY FOR QUESTION
	$(\'body\').on(\'click\', \'.subcatname.active\', function(e) {
	   e.preventDefault();
	   var subcatid = $(this).attr("subcatid");
	   var catid = $(this).attr("catid");
	   DisplayQuestions(catid, subcatid);
	   window.history.pushState("'._('Howto').' : '._('subcat-title').' "+subcatid, "'._('Howto').' : '._('cat-title').' "+catid, getBaseUrl()+"?cat="+catid+"&subcat="+subcatid);
	});


	//**********
	// Display/Close Question modal
	//**********

	function DisplayModal(questionid, callback) {
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.95;
		var dWidth = 1200;
		var wHeight = $(window).innerHeight();
		var dHeight = wHeight * 0.95;
		var questiontitle=$(\'#title-\' + questionid).html();
		//console.log(questiontitle);
		$(\'#openModal-\' + questionid).css({"opacity":"1","pointer-events":"auto"});
		$(\'#openModal-\' + questionid).dialog({
			width: dWidth,
			height: dHeight,
			title: questiontitle
			});

		// Update views counter
		$.ajax({
			url : \'ajax.php\',
			data: {
				incrementview: questionid,
				},
			success: function( data ) {
				console.log("Success incrementing view counter.");
				},
			error: function(data) {
				console.log("Error incrementing view counter : " + data);
				},
			type: \'POST\'
		      	});

		questiondiv = document.getElementById("question-"+questionid);
		$(\'.questiondiv\').removeClass("selected");
		$(questiondiv).addClass("selected");
		$(\'.questiontitle\').hide();
		$(\'h3.acl\').hide();
	}	


	// Update browser URL so it take care of opening the modal
	$(\'body\').on(\'click\',\'.questionname\', function(e) {
		e.preventDefault();
		var questionid = $(this).attr("questionid");
		//alert(questionid);
		DisplayModal(questionid);
		window.history.pushState("'._('Howto').' : '._('question-title').' "+questionid, "'._('Howto').' : '._('question-title').' "+questionid, getBaseUrl()+"?cat="+getURLParameter("cat")+"&subcat="+getURLParameter("subcat")+"&question="+questionid);
		});
	
	/* In case of the TitleModify form is open while you close the modal, restore it */
	$(\'body\').on(\'dialogclose\', \'.modalDialog\', function(){
		var questionid=$(this).closest(\'.modalDialog\').attr(\'questionid\');
		var questiontitle = $(\'#title-\' + questionid).parent().prev().find(".ui-dialog-title").html();
		$(\'#title-\' + questionid).html(questiontitle);
		$(\'#title-\' + questionid).hide();
	 });

	// Update browser URL so it take care of closing the modal
	$(\'body\').on(\'click\',\'.ui-dialog-titlebar-close\', function(e) {
		e.preventDefault();
		var questionid = $(this).parent().next(".modalDialog").attr("questionid");
		//alternatively, use : $(obj).closest(\'tr\').nextAll(\':has(.class):first\')
		//alert(questionid);
	   	//HideModal(questionid);
	   	window.history.pushState("'._('Howto').' : '._('subcat-title').' "+getURLParameter("subcat"), "'._('Howto').' : '._('subcat-title').' "+getURLParameter("subcat"), getBaseUrl()+"?cat="+getURLParameter("cat")+"&subcat="+getURLParameter("subcat"));
	
	});
	//*************
	// QUESTION MODAL
	//*************

	
	// CLICK ADD A GROUP
	//****
	$(document.body).on(\'click\',\'.AddGroup\', function() {
		var questionid=$(this).closest(\'.modalDialog\').attr(\'questionid\');
		var group=$(\'input#searchgroup-\'+questionid).val();
		//-debug-console.log("Group:"+group);
		var others=$(\'#inputacl-\'+questionid).val();
		//-debug-console.log("Other groups:"+others);
		if (others == "") {
			console.log("Clearing group list");
			$(\'#inputacl-\'+questionid).val(group+";");
			}
		else if (others.search(group+";") == "-1") {
			console.log("Adding to group list because search returns :"+others.search(group+";"));
			if (others.slice(-1)!=";")
				$(\'#inputacl-\'+questionid).val(others+";"+group+";");
			else
				$(\'#inputacl-\'+questionid).val(others+group+";");
			}
		else {
			console.log("Warning : The group is already in the group list");
			}
		// Clearing input
		$(\'input#searchgroup-\'+questionid).val("");
		
	});

	// CLICK REMOVE A GROUP
	//****
	$(document.body).on(\'click\',\'.RemGroup\', function() {
		var questionid=$(this).closest(\'.modalDialog\').attr(\'questionid\');
		var group=$(\'input#searchgroup-\'+questionid).val();
		//-debug-console.log("Group:"+group);
		var others=$(\'#inputacl-\'+questionid).val();
		//-debug-console.log("Other groups:"+others);

		if (others.search(group+";") != "-1" && group!="") {
			console.log("Deleting group cause search return : "+others.search(group+";"));
			$(\'#inputacl-\'+questionid).val(others.replace(group+";",""));
			}
		else
			console.log("Error : the group is NOT in the group list");
		// Clearing input
		$(\'input#searchgroup-\'+questionid).val("");

	});

	// *****
	// SEARCH BY GROUP NAME IN LDAP
	// *****
	$(document.body).on(\'focus\', \'input.searchgroup\' ,function(){
	$(this).autocomplete({
		source: function( request, response ) {
		//-debug-console.log(request.term)
		$.ajax({
			url : \'ajax.php\',
			dataType: "jsonp",
			data: {
				search_group: request.term,
				json: true
				},
			success: function( data ) {
				if (data!=null)
					response(data);
				},
			error: function(data) {
				console.log("Error Ajax JSON" + data);
				},
			type: \'POST\'
		      	});
		},
		autoFocus: true,
		minLength: 0     
	});
	});

	// Click on the bouton toggle CKeditor ON
	$(\'body\').on(\'click\', \'.questionmodify\', function() {

		var questionval = $(this).attr("id");
		if (questionval == null) return; // exit if no id on the title
		var questionarr = questionval.split("-");
		var questionid = questionarr[2];
		// Replace the <textarea id="questioncontent-$id"> with a CKEditor
		// instance, using default configuration.
		ck[questionid]=CKEDITOR.replace( \'questioncontent-\' + questionid, { customConfig: \'../js/ckeditor-fr.js\'});
		//alert(ck[questionid]);
		
		$(\'#SaveButton-top-\'+questionid).show();
		$(\'#SaveButton-bottom-\'+questionid).show();
      		$(\'#questionmodify-top-\'+questionid).hide();
      		$(\'#questionmodify-bottom-\'+questionid).hide();
	});


	// Click on the title toggle title to form
	$(\'body\').on(\'click\', \'.ui-dialog-title\', function() {
		var questionid = $(this).parent().next(".modalDialog").attr("questionid");
		var inputvaltitle=$(this).find("input.edit-title").val();
		if ($("#title-"+questionid).hasClass("admin") && $(\'#edit-title-\'+ questionid).length==0) {
			//-debug-console.log("Valeur de inputvaltitle : "+inputvaltitle);
			if (inputvaltitle == null) {
				var inputvalacl=$(this).next(\'.inputacl\').val();

				var questiontitle=$(\'#title-\' + questionid).html();
				$(\'#title-\' + questionid).html(\''._('Title').' :<br /><input type="text" class="edit-title" id="edit-title-\'+ questionid +\'" value="\'+ questiontitle +\'">\');
				$(\'#title-\' + questionid).show();
				$(this).parent().next(".modalDialog").children().next("h3.acl").show();
				$(\'h3.acl\').show();
				}
		} else {
			console.log("Forbidden");
		}
	});


	// Click on the SAVE button (Title)
	$(\'body\').on(\'click\', \'.modalDialog .savebuttontitle\', function() {
		var questionval = $(this).closest(".modalDialog").attr("id");
		var questionarr = questionval.split("-");
		var questionid = questionarr[1];
		var questiontitle=$("input#edit-title-" + questionid).val();
		var questionacl=$("input#inputacl-" + questionid).val();
		//alert(questionacl);
		$.ajax({
			url: \'ajax.php\',
			data: {
				questionid: questionid,
				questiontitle: questiontitle,
				questionacl:questionacl
			},
			error: function(html) {
				console.log("Error saving title & acl");
			},
			success: function(html) {
				//-debug-console.log("Success saving title & acl" + html);
				//$(\'#SaveButtonTitle-\' + questionid).hide();
				$(\'h3.acl\').hide();
				$(\'#title-\' + questionid).html(questiontitle);
				$(\'#title-\' + questionid).parent().prev().find(".ui-dialog-title").html(questiontitle);
				$(\'#title-\' + questionid).hide();
			},
				type: \'POST\',
				timeout : 5000
			});
		});


	// Click on the SAVE button (content)
	$(\'body\').on(\'click\', \'.modalDialog .savebutton\', function() {
		var questionval = $(this).closest(".modalDialog").attr("id");
		var questionarr = questionval.split("-");
		var questionid = questionarr[1];
		//alert(questionid);
		var questionhtml=ck[questionid].getData();
		//alert("id : " + questionid + " HTML : " + questionhtml);
		$.ajax({
			url: \'ajax.php\',
			data: {
				questionid: questionid,
				questionhtml: questionhtml
			},
			error: function(html) {
				console.log("Error saving data");
				$(\'#questioncontent-\' + questionid).html(html);
			},
			success: function(html) {
				//-debug-console.log("Success saving data" + html);
				ck[questionid].destroy();
				//$(\'.savebutton\').hide();
				$(\'#SaveButton-top-\'+questionid).hide();
				$(\'#SaveButton-bottom-\'+questionid).hide();
		      		$(\'#questionmodify-top-\'+questionid).show();
		      		$(\'#questionmodify-bottom-\'+questionid).show();
			},
			type: \'POST\',
			timeout : 5000
		});
	});

   // ************
   // CAT
   // ************
   
   // Display div
   $(\'body\').on(\'click\', \'.modifycatadminsbutton\', function() {
	var catid = $(this).attr("catid");
	$(\'#catadminsdiv-\' + catid).show(); // Show input
	$(\'#savecatadminsbutton-\' + catid).show(); // Show save button
	$(this).hide(); // Hide button
	});
   
   // Trigger click Enter key
   $(\'body\').on(\'keypress\',\'[id^=catadminsinput-]\',function(e) {
      var catid = $(this).attr("catid");
      var key = e.which;
      if(key == 13)  // the enter key code
         {
            $(\'#savecatadminsbutton-\'+catid).click();
            return false;  
         }
      });
   
   // Save changes on click
   $(\'body\').on(\'click\', \'.savecatadminsbutton\', function() {
      var catid = $(this).attr("catid");
      var admins = $("#catadminsinput-"+catid).val();
      //alert(admins);
      $.ajax({
			url: \'ajax.php\',
			data: {
				cat: catid,
				admins : admins
			},
			error: function(html) {
				//alert(html);
				console.log(\'An error (5) has occurred : \' + html);
			},
			success: function(html) {
               $(\'#catadminsdiv-\' + catid).hide();
               $(\'#modifycatadminsbutton-\' + catid).show(); // Show save button
               $(\'#savecatadminsbutton-\' + catid).hide(); // Hide button
					},
         type: \'GET\',
         timeout : 5000
         });
	});

	//*************
	// READ URL AND SELECT CAT IF REQUESTED IN URL
	//*************

	function GetCatFromSubcatId(subcatid, callback) {
		$.ajax({
			url: \'ajax.php\',
			data: {
				subcatid : subcatid,
				getcatid : true
				},
			error: function(html) {
				//alert(html);
				console.log(\'An error () has occurred : \' + html);
				},
			success: function(html) {
				catid= parseInt(html);
				//console.log(html);
			        callback && callback();
			},
		type: \'POST\',
		timeout : 5000
		});
	}
	function GetSubCatFromQuestionId(questionid, callback) {
		$.ajax({
			url: \'ajax.php\',
			data: {
				questionid : questionid,
				getsubcatid : true
				},
			error: function(html) {
				//alert(html);
				console.log(\'An error () has occurred : \' + html);
				},
			success: function(html) {
				//console.log("Dans la fonction" + parseInt(html));
				subcatid = parseInt(html);
			        callback && callback();
			},
		type: \'POST\',
		timeout : 5000
		});
	}


	// Function to analyse URL and load requested information
	function loadpageonurl2() {
		questionid = getURLParameter(\'question\');
		// If All informations is provided, load chat requested
		if (getURLParameter(\'subcat\')!=null && getURLParameter(\'cat\')!=null && getURLParameter(\'question\')!=null) {
			catid=getURLParameter(\'cat\');
			DisplaySubCat(catid, function(){
				subcatid=getURLParameter(\'subcat\');
				DisplayQuestions(catid,subcatid, function(){
					questionid=getURLParameter(\'question\');
					DisplayModal(questionid);
					});
				});
			}
		// If only Cat (and/or Subcat) provided, load them
		else if (getURLParameter(\'question\')==null) {
			if (getURLParameter(\'cat\')!=null) {
				catid=getURLParameter(\'cat\');
				DisplaySubCat(catid, function(){
					if (getURLParameter(\'subcat\')!=null) {
						subcatid=getURLParameter(\'subcat\');
						DisplayQuestions(catid,subcatid);
						}
					});
				}
			else if (getURLParameter(\'subcat\')!=null) {
				subcatid=getURLParameter(\'subcat\');
				GetCatFromSubcatId(subcatid, function() {
					DisplaySubCat(catid, function(){
						DisplayQuestions(catid,subcatid);
						});
					});
				}
			}
		// If only question provided, get corresponding cat and subcat and load them
		else if (getURLParameter(\'question\')!=null) {
			GetSubCatFromQuestionId(questionid, function() {
				GetCatFromSubcatId(subcatid, function() {
					DisplaySubCat(catid, function(){
						DisplayQuestions(catid,subcatid, function(){
							DisplayModal(questionid);
							});
						});
					});
				});
			}
		}

	// Adapt Display for CAT list
	var rowCount = $(\'.cat\').length;
	size = Math.round(screen.height / (rowCount * 4.5));
	realsize=Math.min(size,maxsize);
	$(\'.cat\').css({"line-height":realsize+"px","font-size":realsize+"px"});
	
	loadpageonurl2();

});

	// **************
	// LOADING DIV
	// **************
	// Replace greeter by loading message while doing an ajax request. And restore it after.
	$(document)
	.ajaxStart(function () {
		greeter=$(\'#greeter\').html();
		$(\'#greeter\').html(\''._('Loading').'...\');
		})
	.ajaxStop(function () {
		$(\'#greeter\').html(greeter);
	});

</script>
';
?>
