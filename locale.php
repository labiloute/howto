<?php
$codeset = "utf8";  // warning ! not UTF-8 with dash '-' 
//$locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
$bl = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0]; # get Browser Locale
//echo $bl;
if ($CONFIG['force_locale']) {$locale = $CONFIG['force_locale'];}
else if (strpos($bl,"-")) { # Convert it to unix-like system locale
   $splitted_bl = explode("-",$bl);
   $locale = $splitted_bl[0]."_".$splitted_bl[1];
}
else if (strlen($bl)==2) {
   $locale = strtolower($bl)."_".strtoupper($bl); # Convert fr to fr_FR
}
else
   $locale = $bl; # Else it is already well formed
//echo $locale;
$r = putenv("LC_ALL=".$locale);
if (!$r) {echo "Failed putenv";}
$r = setlocale(LC_TIME, "");
$r = setlocale(LC_ALL, $locale.'.'.$codeset);
if (!$r) $r = setlocale(LC_ALL, 'fr_FR.'.$codeset); # If it still fail force french because it is my native language ;)
if (!$r) $r = setlocale(LC_ALL, 'en_US.'.$codeset); # If everything fail force US english
//echo "<br />";
//echo setlocale(LC_ALL, '0');
if (!$r) {echo "Failed setlocale";}
$domain="messages";
$r = bindtextdomain($domain, "locale");
if (!$r) {echo "Failed bindtext";}
$r = bind_textdomain_codeset($domain, 'UTF-8');
if (!$r) {echo "Failed codeset";}
$r=textdomain($domain);
if (!$r) {echo "Failed textdomain";}
?>
