Simple HOWTO
-----------
<br />
Destination public : App has been designed to be used by very basic users but require a Sys Admin for install<br />
Required : basic mysql skills, basic php<br /><br />
<br />
1- Copy howto folder into your web server (or clone it via git)     <br />
2- Import database howto-vX.sql with your favorite mysql client   <br />
3- Copy file config.php.default to config.php and adapt it to your mysql config.   <br />
4- Browse http://webserver/howto   <br />
5- A default cat named "MYNEWCAT" has been created. Try to create a subcat and a question   <br />
6- Add your categories into your database with phpmyadmin or any other mysql client. Categories and Category admins are the only things to add directly in database <br />
7- Add your subcats and questions via web interface   <br />
8- Enjoy !   <br />

<br />
Please report issues !
<br />
